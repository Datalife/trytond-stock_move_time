# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import stock
from . import ir


def register():
    Pool.register(
        stock.Move,
        stock.ShipmentOut,
        stock.ShipmentOutReturn,
        stock.ShipmentInternal,
        stock.ShipmentInReturn,
        ir.Date,
        module='stock_move_time', type_='model')
