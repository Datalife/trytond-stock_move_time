# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
import dateutil.tz
from datetime import datetime
from trytond.pool import PoolMeta


class Date(metaclass=PoolMeta):
    __name__ = 'ir.date'

    @classmethod
    def localize(cls, value, timezone):
        assert isinstance(value, datetime)
        lzone = (dateutil.tz.gettz(timezone)
            if timezone else dateutil.tz.tzutc())
        szone = dateutil.tz.tzutc()

        if lzone == szone:
            return value
        return value.replace(tzinfo=szone).astimezone(
            lzone).replace(tzinfo=None)
