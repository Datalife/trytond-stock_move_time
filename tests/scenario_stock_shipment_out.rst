===========================
Stock Shipment Out Scenario
===========================

Imports::

    >>> import datetime
    >>> from trytond.tests.tools import activate_modules
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from proteus import Model, Wizard
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> today = datetime.date.today()
    >>> yesterday = today - relativedelta(days=1)
    >>> now = datetime.datetime.now()

Install stock Module::

    >>> config = activate_modules('stock_move_time')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Reload the context::

    >>> User = Model.get('res.user')
    >>> config._context = User.get_preferences(True, config.context)

Create customer::

    >>> Party = Model.get('party.party')
    >>> customer = Party(name='Customer')
    >>> customer.save()

Create category::

    >>> ProductCategory = Model.get('product.category')
    >>> category = ProductCategory(name='Category')
    >>> category.save()

Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> product = Product()
    >>> template = ProductTemplate()
    >>> template.name = 'Product'
    >>> template.categories.append(category)
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.list_price = Decimal('20')
    >>> template.cost_price = Decimal('8')
    >>> template.save()
    >>> product.template = template
    >>> product.save()

Get stock locations::

    >>> Location = Model.get('stock.location')
    >>> warehouse_loc, = Location.find([('code', '=', 'WH')])
    >>> supplier_loc, = Location.find([('code', '=', 'SUP')])
    >>> customer_loc, = Location.find([('code', '=', 'CUS')])
    >>> output_loc, = Location.find([('code', '=', 'OUT')])
    >>> input_loc, = Location.find([('code', '=', 'IN')])
    >>> storage_loc, = Location.find([('code', '=', 'STO')])

Create Shipment Out::

    >>> ShipmentOut = Model.get('stock.shipment.out')
    >>> shipment_out = ShipmentOut()
    >>> shipment_out.start_date = now - relativedelta(minutes=30)
    >>> shipment_out.start_time != None
    True
    >>> shipment_out.end_date = now
    >>> shipment_out.interval != None
    True
    >>> shipment_out.customer = customer
    >>> shipment_out.warehouse = warehouse_loc
    >>> shipment_out.company = company

Add two shipment lines of same product::

    >>> StockMove = Model.get('stock.move')
    >>> shipment_out.outgoing_moves.extend([StockMove(), StockMove()])
    >>> for move in shipment_out.outgoing_moves:
    ...     move.product = product
    ...     move.uom =unit
    ...     move.quantity = 1
    ...     move.from_location = output_loc
    ...     move.to_location = customer_loc
    ...     move.company = company
    ...     move.unit_price = Decimal('1')
    ...     move.currency = company.currency
    >>> shipment_out.save()

Check date fields::

    >>> shipment_out.start_time == shipment_out.start_date.time()
    True
    >>> shipment_out.interval == shipment_out.end_date - shipment_out.start_date
    True

Set the shipment state to waiting::

    >>> shipment_out.click('wait')
    >>> len(shipment_out.outgoing_moves)
    2
    >>> len(shipment_out.inventory_moves)
    2
    >>> shipment_out.outgoing_moves[0].start_date == shipment_out.end_date
    True
    >>> shipment_out.outgoing_moves[0].end_date == shipment_out.end_date
    True
    >>> inventory_move = shipment_out.inventory_moves[0]
    >>> inventory_move = StockMove(inventory_move.id)
    >>> inventory_move.start_date == shipment_out.start_date
    True
    >>> inventory_move.end_date == shipment_out.end_date
    True

Create Shipment Out return::

    >>> ShipmentOutReturn = Model.get('stock.shipment.out.return')
    >>> shipment_out_return = ShipmentOutReturn()
    >>> shipment_out_return.start_date = now - relativedelta(minutes=30)
    >>> shipment_out_return.start_time != None
    True
    >>> shipment_out_return.end_date = now
    >>> shipment_out_return.interval != None
    True
    >>> shipment_out_return.customer = customer
    >>> shipment_out_return.warehouse = warehouse_loc
    >>> shipment_out_return.company = company

    >>> shipment_out_return.incoming_moves.extend([StockMove(), StockMove()])
    >>> for move in shipment_out_return.incoming_moves:
    ...     move.product = product
    ...     move.uom =unit
    ...     move.quantity = 1
    ...     move.from_location = customer_loc
    ...     move.to_location = input_loc
    ...     move.company = company
    ...     move.unit_price = Decimal('1')
    ...     move.currency = company.currency
    >>> shipment_out_return.save()

Check date fields::

    >>> shipment_out_return.start_time == shipment_out_return.start_date.time()
    True
    >>> shipment_out_return.interval == shipment_out_return.end_date - shipment_out_return.start_date
    True
    >>> shipment_out_return.click('receive')
    >>> len(shipment_out_return.incoming_moves)
    2
    >>> len(shipment_out_return.inventory_moves)
    2
    >>> shipment_out_return.incoming_moves[0].start_date == shipment_out_return.start_date
    True
    >>> shipment_out_return.incoming_moves[0].end_date == shipment_out_return.end_date
    True
    >>> inventory_move = shipment_out_return.inventory_moves[0]
    >>> inventory_move = StockMove(inventory_move.id)
    >>> inventory_move.start_date == shipment_out_return.end_date
    True
    >>> inventory_move.end_date == shipment_out_return.end_date
    True