=================================
Stock Shipment In Return Scenario
=================================

Imports::

    >>> import datetime
    >>> from trytond.tests.tools import activate_modules
    >>> from proteus import Model, Wizard, Report
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> now = datetime.datetime.now()


Install stock_move_time Module::

    >>> config = activate_modules('stock_move_time')


Create company::

    >>> _ = create_company()
    >>> company = get_company()


Create Supplier::

    >>> Party = Model.get('party.party')
    >>> supplier = Party(name='Supplier')
    >>> supplier.save()


Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> product = Product()
    >>> template = ProductTemplate()
    >>> template.name = 'Product'
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.list_price = Decimal('20')
    >>> template.cost_price = Decimal('8')
    >>> template.save()
    >>> product.template = template
    >>> product.save()


Get stock locations::

    >>> Location = Model.get('stock.location')
    >>> storage_loc, = Location.find([('code', '=', 'STO')])


Create a shipment in return::

    >>> ShipmentInReturn = Model.get('stock.shipment.in.return')
    >>> shipment_in_return = ShipmentInReturn()
    >>> shipment_in_return.start_date = now - relativedelta(minutes=30)
    >>> shipment_in_return.start_time != None
    True
    >>> shipment_in_return.end_date = now
    >>> shipment_in_return.interval != None
    True
    >>> shipment_in_return.supplier = supplier
    >>> shipment_in_return.from_location = storage_loc
    >>> shipment_in_return.company = company
    >>> move = shipment_in_return.moves.new()
    >>> move.product = product
    >>> move.uom = unit
    >>> move.quantity = 1
    >>> move.from_location = storage_loc
    >>> move.to_location = shipment_in_return.to_location
    >>> move.company = company
    >>> move.unit_price = Decimal('1')
    >>> move.currency = company.currency
    >>> shipment_in_return.save()


Check date fields::

    >>> shipment_in_return.start_time == shipment_in_return.start_date.time()
    True
    >>> shipment_in_return.interval == shipment_in_return.end_date - shipment_in_return.start_date
    True
    >>> move, = shipment_in_return.moves
    >>> move.start_date == shipment_in_return.end_date
    True
    >>> move.end_date == shipment_in_return.end_date
    True
